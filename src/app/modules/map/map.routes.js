mapRoutes.$inject = ['routerHelper'];

/** @ngInject */
export default function mapRoutes(routerHelper) {
  routerHelper.configureStates([
    {
      state: 'map',
      config: {
        url: '/map',
        component: 'simpleMapContainer',
        title: 'Map'
      }
    }
  ]);
}
