import angular from 'angular';
import {landingRoutes} from './landing.routes';
import {landingContainer} from './landing.container';

export const landingModule = 'landing';

angular
  .module(landingModule, [])
  .run(landingRoutes)
  .component('landingContainer', landingContainer);
