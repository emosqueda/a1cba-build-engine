import angular from 'angular';
import {shellContainer} from './shell.container.js';
import {shellPresentation} from './shell.presentation.js';

import {footerContainer} from './footer.container';
import {headerContainer} from './header.container';

export const shellModule = 'shell';

angular
  .module(shellModule, [])
  .component('shellContainer', shellContainer)
  .component('shellPresentation', shellPresentation)
  .component('headerContainer', headerContainer)
  .component('footerContainer', footerContainer);
