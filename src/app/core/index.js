import angular from 'angular';
import 'angular-ui-router';
import {routerHelperProvider} from './routerHelper.provider';
import coreRoutes from './core.routes';
import coreConfig from './core.config';

export const coreModule = 'core';

angular
  .module(coreModule, ['ui.router'])
  .provider('routerHelper', routerHelperProvider)
  .config(coreConfig)
  .run(coreRoutes);
