import {merge as mergeSvc} from 'angular';

class SimpleMapContainerController {
  /** @ngInject */
  constructor($http, $window, $q) {
    this._$http = $http;
    this._$window = $window;
    this._$q = $q;

    this.tilesUrlTemplate = '';
    this.tilesUrlConfig = {};
    this.mapCoords = {};
    this.containerHeight = 0;
  }

  $onInit() {
    this._$http
      .get('/mapApiConfig.json')
      .then(response => {
        const configObj = response.data;
        this.tilesUrlTemplate = configObj.urlTemplate;
        this.tilesUrlConfig = {
          attribution: configObj.attributionString,
          id: configObj.defaultMapId,
          accessToken: configObj.accessToken
        };
      });
    this.containerHeight = 200;
    this.goToMyGeolocation();
  }

  changeMapStyle(newStyle) {
    this.tilesUrlConfig = mergeSvc({}, this.tilesUrlConfig, {id: newStyle});
  }

  changeMapHeight(newHeight) {
    this.containerHeight = newHeight;
    console.log('I changed the height prop');
  }

  changeMapCoords(newLat, newLong, newZoom = 1) {
    this.mapCoords = mergeSvc({}, {
      lat: newLat,
      long: newLong,
      zoom: newZoom
    });
  }

  goToMyGeolocation() {
    this.geolocPromiseWrapper()
      .then(coords => {
        this.mapCoords = mergeSvc({}, this.mapCoords, coords);
      });
  }

  geolocPromiseWrapper() {
    const deferred = this._$q.defer();
    try {
      this._$window.navigator.geolocation.getCurrentPosition(position => {
        deferred.resolve({
          lat: position.coords.latitude,
          long: position.coords.longitude,
          zoom: 10
        });
      });
    } catch (e) {
      deferred.reject(e);
    }
    return deferred.promise;
  }
}

export const simpleMapContainer = {
  template: `
    <simple-map-presentation
      container-height="{{$ctrl.containerHeight}}"
      tiles-url-template="$ctrl.tilesUrlTemplate"
      map-coords="$ctrl.mapCoords"
      tiles-url-config="$ctrl.tilesUrlConfig"
      ></simple-map-presentation>
    <button ng-click="$ctrl.changeMapHeight(400)">Go 400px</button>
    <button ng-click="$ctrl.goToMyGeolocation()">Go to my Geoloc</button>
  `,
  controller: SimpleMapContainerController
};
