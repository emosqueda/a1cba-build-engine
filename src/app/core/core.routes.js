coreRoutes.$inject = ['routerHelper'];

/** @ngInject */
export default function coreRoutes(routerHelper) {
  const otherwise = '/404';
  routerHelper.configureStates([
    {
      state: '404',
      config: {
        url: '/404',
        template: `<h1>This is spartan 404</h1>`,
        title: '404'
      }
    }
  ], otherwise);
}
