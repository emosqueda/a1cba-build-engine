import angular from 'angular';

routerHelperProvider.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider'];

/** @ngInject */
export function routerHelperProvider($locationProvider, $stateProvider, $urlRouterProvider) {
  const config = {
    docTitle: undefined,
    resolveAlways: {}
  };

  // function checkBrowserHistory($window) {
  //   if (!($window.history && $window.history.pushState)) {
  //     $window.location.hash = '/';
  //   }
  // }

  $locationProvider.html5Mode(true).hashPrefix('!');

  this.configure = cfg => {
    angular.extend(config, cfg);
  };

  this.$get = RouterHelper;

  RouterHelper.$inject = ['$location', '$rootScope', '$state', '$log'];
  /** @ngInject */
  function RouterHelper($location, $rootScope, $state, $log) {
    const stateCounts = {
      errors: 0,
      changes: 0
    };
    let handlingStateChangeError = false;
    let hasOtherwise = false;

    const service = {
      configureStates,
      getStates,
      stateCounts
    };

    init();

    return service;

    // /////////////

    function init() {
      console.log('about to init');
      handleRoutingErrors();
      updateDocTitle();
    }

    function configureStates(states, otherwisePath) {
      states.forEach(state => {
        state.config.resolve = angular.extend(state.config.resolve || {}, config.resolveAlways);
        $stateProvider.state(state.state, state.config);
      });
      if (otherwisePath && !hasOtherwise) {
        hasOtherwise = true;
        $urlRouterProvider.otherwise(otherwisePath);
      }
    }

    function getStates() {
      return $state.get();
    }

    function handleRoutingErrors() {
      // Route cancellation:
      // On routing error, go to the dashboard.
      // Provide an exit clause if it tries to do it twice.
      const stateErrorDesreg = $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
        if (handlingStateChangeError) {
          return;
        }
        stateCounts.errors++;
        handlingStateChangeError = true;
        const destination = (toState && (toState.title || toState.name || toState.loadedTemplateUrl)) || 'unknown target';
        const msg = `
            Error routing to ${destination}. ${error.data || ''}
            <br/>
            ${error.statusText || ''}: ${error.status || ''}+
          `;
        $log.warn(msg, [toState]);
        $location.path('/');
      });

      const destroyDesreg = $rootScope.$on('destroy', () => {
        console.log('destroyer error');
        stateErrorDesreg();
        destroyDesreg();
      });
    }

    function updateDocTitle() {
      const stateSuccessDesreg = $rootScope.$on('$stateChangeSuccess', (event, toState) => {
        stateCounts.changes++;
        handlingStateChangeError = false;
        console.log('about to change the docTitle', event, toState);
        $rootScope.title = `${config.docTitle} ${toState.title || ''}`; // data bind to <title>
      });

      const destroyDesreg = $rootScope.$on('$destroy', () => {
        console.log('destroyer success');
        stateSuccessDesreg();
        destroyDesreg();
      });
    }
  }
}
