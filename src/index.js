import angular from 'angular';
import 'angular-ui-router';
import {coreModule} from './app/core/index';
import {shellModule} from './app/shell/index';

import {landingModule} from './app/modules/landing/index';
import {mapModule} from './app/modules/map/index';

import './index.scss';

angular
  .module('app', [
    shellModule, coreModule, mapModule, landingModule
  ]);
