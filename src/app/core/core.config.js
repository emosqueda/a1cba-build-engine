coreConfig.$inject = ['$logProvider', 'routerHelperProvider'];

export default function coreConfig($logProvider, routerHelperProvider) {
  const config = {
    appErrorPrefix: '[A1-CTA Hiccup] ',
    appTitle: 'A1-CTA'
  };
  if ($logProvider.debugEnabled) {
    $logProvider.debugEnabled(true);
  }
  routerHelperProvider.configure({
    docTitle: `${config.appTitle}: `
  });
}

/**
 * Initialize the config value to be kept thorough the execution
 * insertable as a value
 * @type {Object}
 */
// var config = {
//   appErrorPrefix: '[wisedude Hiccup] ',
//   appTitle: 'Wisedude'
// };

// core.value('config', config);

// configure.$inject = ['$logProvider', 'routerHelperProvider', 'exceptionHandlerProvider'];
//
/* @ngInject */
// function configure($logProvider, routerHelperProvider, exceptionHandlerProvider) {
//   if ($logProvider.debugEnabled) {
//     $logProvider.debugEnabled(true);
//   }
//   exceptionHandlerProvider.configure(config.appErrorPrefix);
//   routerHelperProvider.configure({
//     docTitle: config.appTitle + ': '
//   });
// }
