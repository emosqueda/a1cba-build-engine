class ShellContainerController {
  /** @ngInject */
  constructor($rootScope) {
    console.log('no useless constructor', $rootScope);
  }
}

export const shellContainer = {
  template: `
    <header-container></header-container>
    <section id="main">
      <shell-presentation></shell-presentation>
    </section>
    <footer-container></footer-container>
  `,
  controller: ShellContainerController
};
