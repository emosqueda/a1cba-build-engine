import L from 'leaflet';
import {element as elementSvc} from 'angular';

class simpleMapPresentationController {
  /** @ngInject */
  constructor($log, $element) {
    this._$log = $log;
    this._$element = $element;

    this.containerElement = null;
    this.containerElement = elementSvc('<div/>')[0];
    this.mapInstance = null;
  }

  $onInit() {
    this.setMapHeight();
    this.instantiateMap();
    this.setMapStyle();
    this.setView();
  }

  $onChanges(changesObj) {
    if (changesObj.tilesUrlConfig || changesObj.tilesUrlTemplate) {
      this.setMapStyle(this.tilesUrlTemplate, this.tilesUrlConfig);
    }
    if (changesObj.containerHeight) {
      this.setMapHeight();
    }
    if (changesObj.mapCoords) {
      this.setView();
    }
  }

  instantiateMap() {
    this._$element[0].appendChild(this.containerElement);
    this.mapInstance = L
      .map(this.containerElement);
  }

  setView() {
    if (this.mapInstance && this.mapCoords.lat && this.mapCoords.long) {
      this.mapInstance.setView([this.mapCoords.lat, this.mapCoords.long], this.mapCoords.zoom);
    }
  }

  setMapStyle() {
    if (this.tileLayer) {
      this.tileLayer.remove();
    }
    this.tileLayer = L
      .tileLayer(this.tilesUrlTemplate, this.tilesUrlConfig);
    if (this.mapInstance) {
      this.tileLayer.addTo(this.mapInstance);
    }
  }

  setMapHeight() {
    if (this.containerElement) {
      this.containerElement.setAttribute('style', `height: ${this.containerHeight}px`);
      if (this.mapInstance) {
        this.mapInstance.invalidateSize();
      }
    }
  }
} // End class

export const simpleMapPresentation = {
  controller: simpleMapPresentationController,
  bindings: {
    tilesUrlTemplate: '<',
    tilesUrlConfig: '<',
    mapCoords: '<',
    containerHeight: '@'
  }
};
