export const footerContainer = {
  template: `
    <footer class="footer">
      Built with ♥ by the portal.appdev team
    </footer>
  `
};
