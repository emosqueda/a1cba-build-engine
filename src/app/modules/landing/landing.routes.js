landingRoutes.$inject = ['routerHelper'];

/** @ngInject */
export function landingRoutes(routerHelper) {
  routerHelper.configureStates([
    {
      state: 'landing',
      config: {
        url: '/',
        component: 'landingContainer',
        title: 'Landing'
      }
    }
  ]);
}
