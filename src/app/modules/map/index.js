import angular from 'angular';
import {simpleMapContainer} from './simpleMap.container';
import {simpleMapPresentation} from './simpleMap.presentation';
import mapRoutes from './map.routes';
export const mapModule = 'map';

angular
  .module(mapModule, [])
  .run(mapRoutes)
  .component('simpleMapContainer', simpleMapContainer)
  .component('simpleMapPresentation', simpleMapPresentation);
